# README #

* This shell was made as a part of OS assignment.
* Author - Akshat Tandon


### Quick summary ###
### It supports the following features. ###
* Foreground and Background process.
* Input/Output redirection
* Pipes

* Version
-1.2


### How do I get set up? ###

* Clone the repo
* Compile main.c
* Execute the program